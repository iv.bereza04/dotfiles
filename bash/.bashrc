[[ $- != *i* ]] && return

# Alias
LS="ls --color"
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'

alias ls="$LS -1F"
alias la="$LS -Alh"
alias ll="$LS -alhF"

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# Prompt
PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h: \[\033[34;1m\]\w\[\033[m\] $ "
